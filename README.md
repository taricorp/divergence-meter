# Hardware

 * 8 x IN-14 Nixie tubes
   * 8 x IN-14 tube holders
 * 1 x [NCH6100HV](http://www.nixieclock.org/?p=493) DC power supply
 * 1 x [Nixie carrier board](nixie-board/)
 * 1 x [Opto board](opto-board/)
 * 1 x [Control board](control-board/)

TODO: better BOMs for the boards

The heart of the whole design is IN-14 Nixie tubes. They haven't been
manufactured for decades, so sourcing them may be a bit of a challenge. I bought
mine in a lot of 9 on eBay, apparently salvaged from some other equipment (the
leads were dirty and bent, but after a little work to straighten and clean them
they're quite serviceable).

The [Unusual Electronics](doc/in14-unusualelectronics.pdf) Nixie datasheet
mentions a plastic spacer; it's unclear to me if that would be part of the
original equipment for use with the tube or it's of their own design, but I've
modelled a [similar part that can easily be 3d printed](in-14-holder.sldprt).
They're useful for keeping the tube leads straight, and lifting them off their
carrier board a little bit.

The NCH6100HV supplies the ~200V necessary to drive the Nixie tubes. While a
power supply could be more tightly integrated with a custom design, I couldn't
be bothered to design a boost converter that meets the needs. Anything capable
of supplying about 10 mA continuously at 180V should be sufficient to drive the
IN-14s.

## Boards

There are three custom PCBs in the design, meant to stack vertically with the
control board on the bottom, with opto board on top of it and the Nixie carrier
board on top of the stack. The connections between each board are designed as
common 0.1" breakaway headers, though they could be wired in some other way.

TODO: check in the control board design

The Nixie carrier board is a simple PCB that the tubes get soldered to and
routes power to them.

The opto board sits between the control and nixie boards, providing galvanic
isolation between the low-voltage control board and high voltages required to
drive the tubes.

## Enclosure

The enclosure is designed to be laser cut from 3mm material (acrylic and plywood
in my instantiation) and fastened with eight each of M3x20mm machine screws and M3
nuts.

TODO: add drawing of the assembly, how nuts fit in the T-slots

While the headers between the boards tend to be sufficient to keep the whole
stack somewhat mechanically sound, there are mounting holes on each of them that
accept M3 hex standoffs. These will be necessary if not using rigid headers, and
are probably a good idea in any case to ensure the correct spacing between
boards- correct spacing is necessary for assembly into the enclosure.

The control board is fastened to a plate in the enclosure with the mounting
hardware, and at the top the nixie board is fastened to the top of the enclosure
with screws dropping through the enclosure and board to mate with the standoffs
that the nixie board depends on.

TODO: add a drawing of the hex standoff assembly, find what size standoffs I
have

To retain the high voltage power supply in its compartment at the bottom of the
case, I [designed a mounting
plate](https://cad.onshape.com/documents/c5792401d2aa944a9cd0700a/w/ab2df73ac10aaaa90ea8715e/e/28225f66fbfe63f120428198)
that can be attached to the bottom piece of the enclosure (perhaps glued or
taped) and 3d-printed. Compared to simply using the power supply's mounting
holes, this approach avoids putting more holes in the enclosure and allows it to
be fastened and removed without access to the top or bottom of the power supply
(because space is fairly tight inside the compartment) as well as avoiding the
need for another kind of hardware (the holes on the board are M2 sized, which is
rather more inconvenient). M3 screws can thread into the captive M3 nuts on the
retaining clips, which in turn fit into the mounting holes on the power supply.

TODO: diagram of how these fit together too.

## Wiring

Wiring of the system is fairly simple. 12V DC input should be wired to the input
of the high voltage supply, and to the supply input for the control board. I've
designed it to use an unremarkable panel-mount barrel jack and rocker switch on
the rear panel of the enclosure so the entire unit can be switched on an off
without unplugging it.

TODO: find the exact Tayda part numbers I have for those

Output from the high voltage supply is wired to the wiring points on the opto
board, bypassing the control board. All remaining electrical connections run
through the board-to-board connections in the stackup.

TODO: am I doing something with ~SHDN?

# Software

It's a work in progress.
