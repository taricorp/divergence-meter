use core::fmt::Write;
use core::ops::Deref;
use core::str::FromStr;
use rtic::Mutex;

use super::rtc::Rtc;

pub trait ConsoleTransport {
    type ReadError;
    type WriteError;

    fn read(&mut self, buf: &mut [u8]) -> Result<usize, Self::ReadError>;
    fn write(&mut self, buf: &[u8]) -> Result<usize, Self::WriteError>;
}

pub struct STM32Serial<USART, TXPIN, RXPIN>(stm32f0xx_hal::serial::Serial<USART, TXPIN, RXPIN>);

impl<USART, TXPIN, RXPIN> From<stm32f0xx_hal::serial::Serial<USART, TXPIN, RXPIN>>
    for STM32Serial<USART, TXPIN, RXPIN>
{
    fn from(serial: stm32f0xx_hal::serial::Serial<USART, TXPIN, RXPIN>) -> Self {
        STM32Serial(serial)
    }
}

impl<USART, TXPIN, RXPIN> ConsoleTransport for STM32Serial<USART, TXPIN, RXPIN>
where
    USART: Deref<Target = stm32f0xx_hal::stm32::usart1::RegisterBlock>,
    TXPIN: stm32f0xx_hal::serial::TxPin<USART>,
    RXPIN: stm32f0xx_hal::serial::RxPin<USART>,
{
    type ReadError = stm32f0xx_hal::serial::Error;
    type WriteError = core::convert::Infallible;

    fn read(&mut self, buf: &mut [u8]) -> Result<usize, Self::ReadError> {
        use stm32f0xx_hal::prelude::*;
        for (ofs, byte) in buf.iter_mut().enumerate() {
            match self.0.read() {
                Ok(x) => *byte = x,
                Err(nb::Error::WouldBlock) => return Ok(ofs),
                Err(nb::Error::Other(e)) => return Err(e),
            }
        }
        Ok(buf.len())
    }

    fn write(&mut self, buf: &[u8]) -> Result<usize, Self::WriteError> {
        use stm32f0xx_hal::prelude::*;
        for (ofs, &byte) in buf.iter().enumerate() {
            match self.0.write(byte) {
                Ok(_) => continue,
                Err(nb::Error::WouldBlock) => return Ok(ofs),
                Err(nb::Error::Other(e)) => return Err(e),
            }
        }
        Ok(buf.len())
    }
}

struct Buffer(heapless::spsc::Queue<u8, heapless::consts::U128, u8, heapless::spsc::MultiCore>);

impl Buffer {
    fn new() -> Self {
        Buffer(heapless::spsc::Queue::u8())
    }

    fn write(&mut self, bytes: &[u8]) -> usize {
        for (i, &byte) in bytes.iter().enumerate() {
            if self.0.enqueue(byte).is_err() {
                return i;
            }
        }
        bytes.len()
    }
}

impl core::fmt::Write for Buffer {
    fn write_str(&mut self, s: &str) -> core::fmt::Result {
        let written = self.write(s.as_bytes());
        if written < s.len() {
            // Not enough space for everything in the buffer
            Err(core::fmt::Error)
        } else {
            Ok(())
        }
    }
}

pub struct Console<T: ConsoleTransport> {
    input_present: usize,
    input_buffer: [u8; 128],
    output: Buffer,
    transport: T,
}

pub enum RunResult<T, R, W> {
    Ok(T),
    Read(R),
    Write(W),
}

impl<T: ConsoleTransport> Console<T> {
    pub fn new<R: Into<T>>(transport: R) -> Self {
        Console {
            input_present: 0,
            input_buffer: [0; 128],
            output: Buffer::new(),
            transport: transport.into(),
        }
    }

    pub fn run<RTC: Mutex<T = Rtc>>(
        &mut self,
        rtc: RTC,
    ) -> RunResult<(), T::ReadError, T::WriteError> {
        let received = match self
            .transport
            .read(&mut self.input_buffer[self.input_present..])
        {
            Ok(x) => x,
            Err(e) => return RunResult::Read(e),
        };
        // Echo input
        for &byte in &self.input_buffer[self.input_present..self.input_present + received] {
            // Expand carriage returns so command output doesn't clobber the echoed input.
            if byte == '\r' as u8 {
                self.output.write(b"\n");
            }
            self.output.write(&[byte]);
        }
        self.input_present += received;

        match self.process_commands(rtc) {
            Ok(x) => RunResult::Ok(x),
            Err(e) => RunResult::Write(e),
        }
    }

    fn process_commands<RTC: Mutex<T = Rtc>>(&mut self, mut rtc: RTC) -> Result<(), T::WriteError> {
        while let Some(newline_idx) = self.input_buffer.iter().position(|&b| b == '\r' as u8) {
            match core::str::from_utf8(&self.input_buffer[..newline_idx]) {
                Ok(line) => {
                    let mut words = line.split(' ');
                    match words.next() {
                        Some("") | None => { /* Don't care */ }
                        Some("time") => {
                            let now = rtc.lock(|t| t.read_time());
                            let (year, month, day, hours, minutes, seconds) = now.decode();
                            let _ = write!(
                                self.output,
                                "{:04}-{:02}-{:02} {:02}:{:02}:{:02}",
                                year, month, day, hours, minutes, seconds
                            );
                        }
                        Some("settime") => match read_time(words) {
                            Ok((y, mon, d, h, min, s)) => {
                                rtc.lock(|t| t.set_time(y, mon, d, h, min, s));
                            }
                            Err(e) => {
                                let _ = write!(
                                    self.output,
                                    "Invalid time (wanted yyyy mm dd hh mm ss): {}",
                                    e
                                );
                            }
                        },
                        Some("readmemory") => {
                            match words.next().and_then(|s| u32::from_str_radix(s, 16).ok()) {
                                Some(addr) => {
                                    let p = addr as *const u32;
                                    let value = unsafe { core::ptr::read_volatile(p) };
                                    let _ =
                                        write!(self.output, "*{:#010x} = {:#010X}", addr, value);
                                }
                                None => {
                                    let _ = write!(self.output, "Expected unprefixed hex address");
                                }
                            }
                        }
                        Some("writememory") => {
                            match words.next().and_then(|s| u32::from_str_radix(s, 16).ok()) {
                                Some(addr) => {
                                    match words.next().and_then(|s| u32::from_str_radix(s, 16).ok())
                                    {
                                        Some(value) => {
                                            let p = addr as *mut u32;
                                            unsafe { core::ptr::write_volatile(p, value) };
                                        }
                                        None => {
                                            let _ = write!(
                                                self.output,
                                                "Expected unprefixed hex value"
                                            );
                                        }
                                    }
                                }
                                None => {
                                    let _ = write!(self.output, "Expected unprefixed hex address");
                                }
                            }
                        }
                        Some(s) => {
                            let _ = write!(self.output, "Unrecognized command: {:?}", s);
                        }
                    }
                }
                Err(e) => {
                    let _ = write!(self.output, "Bad input encoding: {:?}", e);
                }
            }
            self.output.write(b"\r\n> ");

            // Consume the line from the input buffer
            self.input_buffer.copy_within(newline_idx + 1.., 0);
            self.input_present -= newline_idx + 1;
        }

        if self.input_present == self.input_buffer.len() {
            // Failed to consume any input and the buffer is full, so warn and drop.
            self.output.write(b"input overflow\r\n");
            self.input_present = 0;
        }

        self.flush_nonblocking().map(|_| ())
    }

    /// Write as many bytes from the output buffer to the transport as possible,
    /// returning the number of bytes written.
    fn flush_nonblocking(&mut self) -> Result<usize, T::WriteError> {
        let mut written = 0;
        while let Some(&byte) = self.output.0.peek() {
            let wrote_bytes = self.transport.write(&[byte])?;
            // Only dequeue from the buffer if the write was successful, otherwise
            // stop trying.
            if wrote_bytes == 1 {
                self.output.0.dequeue();
                written += 1;
            } else {
                break;
            }
        }

        Ok(written)
    }

    pub fn write_fmt(&mut self, args: core::fmt::Arguments) -> core::fmt::Result {
        self.output.write_fmt(args)?;
        let _ = self.flush_nonblocking();
        Ok(())
    }
}

fn read_time<'a>(
    mut words: impl Iterator<Item = &'a str>,
) -> Result<(u16, u8, u8, u8, u8, u8), &'static str> {
    let mut next = || words.next().ok_or("Not enough values");
    fn parse<T: FromStr>(s: &str) -> Result<T, &'static str> {
        <T as FromStr>::from_str(s).map_err(|_| "Invalid number")
    }

    let out = (
        parse(next()?)?,
        parse(next()?)?,
        parse(next()?)?,
        parse(next()?)?,
        parse(next()?)?,
        parse(next()?)?,
    );
    if !words.next().is_some() {
        Ok(out)
    } else {
        Err("Too many values")
    }
}
