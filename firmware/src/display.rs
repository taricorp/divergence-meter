use heapless::consts::U1;
use heapless::spsc::Consumer;

type TimeRx<'a> = Consumer<'a, (u16, u8, u8, u8, u8, u8), U1, u8>;

pub struct Display<'a> {
    time_rx: TimeRx<'a>,
    display_time: (u16, u8, u8, u8, u8, u8),
}

impl<'a> Display<'a> {
    pub fn setup(time_rx: TimeRx<'a>) -> Self {
        Display {
            time_rx,
            display_time: (0, 0, 0, 0, 0, 0),
        }
    }

    pub fn update(&mut self) {
        // TODO do output stuff

        while let Some(t) = self.time_rx.dequeue() {
            self.display_time = t;
        }
    }
}
