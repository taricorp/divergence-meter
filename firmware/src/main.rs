#![no_std]
#![no_main]

// Panic implementation
extern crate panic_semihosting;

use hal::prelude::*;
use hal::rcc::USBClockSource;
use hal::stm32;
use hal::time::MegaHertz;
use hal::timers::Timer;
use stm32f0xx_hal as hal;

use heapless::consts::U1;
use heapless::spsc::{Producer, Queue};

mod console;
mod display;
mod rtc;

use rtic::app;

#[app(device = stm32f0xx_hal::stm32, peripherals = true)]
const APP: () = {
    struct Resources {
        leds: (
            hal::gpio::gpiob::PB15<hal::gpio::Output<hal::gpio::OpenDrain>>,
            hal::gpio::gpiob::PB14<hal::gpio::Output<hal::gpio::OpenDrain>>,
            hal::gpio::gpiob::PB13<hal::gpio::Output<hal::gpio::OpenDrain>>,
        ),
        rtc: rtc::Rtc,
        tick_timer: Timer<stm32::TIM3>,
        time_tx: Producer<'static, (u16, u8, u8, u8, u8, u8), U1, u8>,
        display: display::Display<'static>,
        /*usb: usb::CDCDevice,*/
        console: console::Console<
            console::STM32Serial<
                stm32::USART2,
                hal::gpio::gpioa::PA14<hal::gpio::Alternate<hal::gpio::AF1>>,
                hal::gpio::gpioa::PA15<hal::gpio::Alternate<hal::gpio::AF1>>,
            >,
        >,
    }

    #[init]
    fn init(cx: init::Context) -> init::LateResources {
        // The display reads this queue to get the current time without contending for
        // access to the RTC, and it's periodically written from a timer task to ensure
        // the display has a pretty accurate time. It's possible for updates to be lost
        // if the display is reading less often than the queue is being written, but
        // application structure ensures this is rare or impossible.
        //
        // While a queue is a bit of a weird choice for this, it's readily available
        // and avoids locking.
        static mut TIME_QUEUE: Queue<(u16, u8, u8, u8, u8, u8), U1, u8> =
            Queue(heapless::i::Queue::u8());

        let _core = cx.core;
        let mut peripherals = cx.device;

        // RCC setup must come before any further peripheral configuration,
        // since this does whatever it wants with clocks.
        let mut rcc = peripherals
            .RCC
            .configure()
            // Default HSI system clock source
            .sysclk(MegaHertz(16))
            .hclk(MegaHertz(16))
            .pclk(MegaHertz(16))
            // USB from HSI48 with CRS trimming from USB SOF
            .usbsrc(USBClockSource::HSI48)
            .enable_crs(peripherals.CRS)
            .freeze(&mut peripherals.FLASH);

        let rtc = rtc::Rtc::configure(&mut rcc, &mut peripherals.PWR, peripherals.RTC);

        let (time_tx, time_rx) = TIME_QUEUE.split();
        let display = display::Display::setup(time_rx);

        let hal::gpio::gpiob::Parts {
            pb13, pb14, pb15, ..
        } = peripherals.GPIOB.split(&mut rcc);
        let mut leds = cortex_m::interrupt::free(|cs| {
            (
                pb15.into_open_drain_output(cs),
                pb14.into_open_drain_output(cs),
                pb13.into_open_drain_output(cs),
            )
        });
        leds.0.set_low().unwrap();
        leds.1.set_high().unwrap();
        leds.2.set_high().unwrap();

        let hal::gpio::gpioa::Parts {
            pa11: _pa11,
            pa12: _pa12,
            pa14,
            pa15,
            ..
        } = peripherals.GPIOA.split(&mut rcc);
        let usart2 = peripherals.USART2;

        // Serial interface on USART2 for the console
        let mut serial = cortex_m::interrupt::free(|cs| {
            hal::serial::Serial::usart2(
                usart2,
                (pa14.into_alternate_af1(cs), pa15.into_alternate_af1(cs)),
                115200.bps(),
                &mut rcc,
            )
        });
        serial.listen(hal::serial::Event::Txe);
        serial.listen(hal::serial::Event::Rxne);

        // Tick the application at 8 Hz on TIM3, passing the time to the display
        // code.
        let mut tick_timer = Timer::tim3(peripherals.TIM3, 8.hz(), &mut rcc);
        tick_timer.listen(hal::timers::Event::TimeOut);
        tick_timer.start(8.hz());

        init::LateResources {
            leds,
            rtc,
            tick_timer,
            time_tx,
            display,
            console: console::Console::new(serial),
            /*usb: usb::CDCDevice::init(peripherals.USB, pa11, pa12),*/
        }
    }

    #[task(binds=TIM3, priority=2, resources=[rtc, tick_timer, time_tx, leds])]
    fn tick(ctx: tick::Context) {
        let tick::Resources {
            rtc,
            tick_timer,
            time_tx,
            leds,
        } = ctx.resources;

        // Required: acknowledge the timer interrupt
        let timer_interrupted = tick_timer.wait().is_ok();
        debug_assert!(timer_interrupted);

        let now = rtc.read_time();
        // Ignore result; if the display is lagging and the queue is full
        // we can't do anything about it.
        let _ = time_tx.enqueue(now.decode());

        let _ = leds.1.toggle();
    }

    #[task(binds=USART2, resources=[console, rtc])]
    fn serial_event(ctx: serial_event::Context) {
        let serial_event::Resources {
            console,
            rtc,
        } = ctx.resources;

        console.run(rtc);
    }
};
