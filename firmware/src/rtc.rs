use stm32f0xx_hal::{self as hal, stm32};

pub struct Rtc(stm32::RTC);

pub struct OpaqueTime {
    tr: stm32::rtc::tr::R,
    dr: stm32::rtc::dr::R,
}

impl OpaqueTime {
    /// Get (year, month, day, hour, minute, second) from an opaque time.
    pub fn decode(&self) -> (u16, u8, u8, u8, u8, u8) {
        let year = 2000 + self.dr.yt().bits() as u16 * 10 + self.dr.yu().bits() as u16;
        let month = if self.dr.mt().bits() { 10 } else { 0 } + self.dr.mu().bits();
        let day = self.dr.dt().bits() * 10 + self.dr.du().bits();
        let hour = self.tr.ht().bits() * 10 + self.tr.hu().bits();
        let minute = self.tr.mnt().bits() * 10 + self.tr.mnu().bits();
        let second = self.tr.st().bits() * 10 + self.tr.su().bits();

        (year, month, day, hour, minute, second)
    }
}

impl Rtc {
    pub fn configure(_rcc: &mut hal::rcc::Rcc, pwr: &mut stm32::PWR, rtc: stm32::RTC) -> Self {
        // The HAL's RCC doesn't allow any access to its registers; inside the HAL
        // crate its regs member can be used, but that's not public. Instead we need
        // to get the regs ourselves, but still take the configured RCC as a parameter
        // to indicate that we mutate the RCC configuration.
        let rcc = unsafe { &*stm32::RCC::ptr() };

        // Enable clocks to power control, where backup domain controls live
        rcc.apb1enr.modify(|_, w| w.pwren().enabled());

        // Enable write access to the backup domain, where the RTC lives
        pwr.cr.modify(|_, w| w.dbp().set_bit());
        // Wait for confirmation to avoid possibly losing writes.
        loop {
            if pwr.cr.read().dbp().bit() {
                break;
            }
        }

        // Only configure if the RTC isn't already running
        let mut rtc = Rtc(rtc);
        if !rcc.bdcr.read().rtcen().is_enabled() {
            rtc.configure_hw(rcc);
        }

        // Leave PWREN and DBP set so the backup domain remains writable and we
        // don't need to touch RCC or PWR for any reason later on.
        rtc
    }

    fn configure_hw(&mut self, rcc: &stm32::rcc::RegisterBlock) {
        // Enable LSE oscillator, wait for it to lock
        rcc.bdcr.write(|w| w.lsedrv().high().lseon().set_bit());
        loop {
            if rcc.bdcr.read().lserdy().bit_is_set() {
                break;
            }
        }
        // Configure RTCCLK to be driven by the LSE
        // If already configured this requires a backup domain reset, but
        // we only ever use LSE so it should be fine.
        rcc.bdcr.modify(|_, w| w.rtcsel().lse().rtcen().enabled());

        // Configure RTC
        // Prescale by (128 * 256) = 32k for 32 kHz LSE
        self.with_writable(|rtc| {
            rtc.enter_init_mode();
            rtc.0
                .prer
                .write(|w| unsafe { w.prediv_a().bits(0x7F).prediv_s().bits(0xFF) });
            rtc.exit_init_mode();
        });
    }

    /// Get the current time from the RTC hardware.
    ///
    /// This function is a minimal accessor, meant to be fast to hold locks for
    /// minimum time.
    pub fn read_time(&mut self) -> OpaqueTime {
        // This function takes &mut self because reading any of the calendar
        // registers locks them until DR is read; the hardware is stateful so
        // accesses must ensure they can't interfere by getting a mutable
        // reference.
        let tr = self.0.tr.read();
        let dr = self.0.dr.read();
        OpaqueTime { tr, dr }
    }

    pub fn set_time(&mut self, year: u16, month: u8, day: u8, hour: u8, minute: u8, second: u8) {
        self.with_writable(|rtc| {
            rtc.enter_init_mode();
            rtc.0.tr.write(|w| unsafe {
                w.ht()
                    .bits((hour / 10) % 10)
                    .hu()
                    .bits(hour % 10)
                    .mnt()
                    .bits((minute / 10) % 10)
                    .mnu()
                    .bits(minute % 10)
                    .st()
                    .bits((second / 10) % 10)
                    .su()
                    .bits(second % 10)
            });
            rtc.0.dr.write(|w| unsafe {
                // bits() requires unsafe, but this is otherwise safe
                w.yt()
                    .bits(((year / 10) % 10) as u8)
                    .yu()
                    .bits((year % 10) as u8)
                    .mt()
                    .bit(month >= 10)
                    .mu()
                    .bits(month % 10)
                    .dt()
                    .bits((day / 10) % 10)
                    .du()
                    .bits(day % 10)
            });
            rtc.exit_init_mode();
            // Clear RSF since shadow registers won't be updated immediately;
            // it will be set by hardware when back in sync.
            rtc.0.isr.modify(|_, w| w.rsf().clear_bit());
        });
    }

    /// Run a function with the RTC unlocked.
    ///
    /// This requires exclusive access to the RTC because the hardware is stateful.
    fn with_writable<T, F: FnOnce(&mut Rtc) -> T>(&mut self, f: F) -> T {
        // Unlock RTC writes
        self.0.wpr.write(|w| unsafe { w.bits(0xCA) });
        self.0.wpr.write(|w| unsafe { w.bits(0x53) });

        let out = f(self);

        // Lock RTC registers
        self.0.wpr.write(|w| unsafe { w.bits(0xFF) });

        out
    }

    fn enter_init_mode(&mut self) {
        self.0.isr.write(|w| w.init().set_bit());
        loop {
            if self.0.isr.read().initf().bit_is_set() {
                break;
            }
        }
    }

    fn exit_init_mode(&mut self) {
        self.0.isr.modify(|_, w| w.init().clear_bit());
    }
}
