use core::mem::MaybeUninit;

use hal::gpio::gpioa::{PA11, PA12};
use hal::stm32;
use hal::usb::{self, UsbBus};
use stm32f0xx_hal as hal;
use usb_device::prelude::*;
use usbd_serial::SerialPort as UsbdSerialPort;

type PinMode = hal::gpio::Input<hal::gpio::Floating>;
pub type SerialPort = UsbdSerialPort<'static, UsbBus<usb::Peripheral>>;
pub type Device = UsbDevice<'static, UsbBus<usb::Peripheral>>;

pub struct CDCDevice {
    pub port: SerialPort,
    device: Device,
}

impl CDCDevice {
    pub fn init(controller: stm32::USB, dm: PA11<PinMode>, dp: PA12<PinMode>) -> CDCDevice {
        // Statically allocate the bus allocator
        let usb_bus = unsafe {
            static mut BUS_ALLOCATOR: MaybeUninit<
                usb_device::bus::UsbBusAllocator<UsbBus<usb::Peripheral>>,
            > = MaybeUninit::uninit();

            BUS_ALLOCATOR = MaybeUninit::new(UsbBus::new(usb::Peripheral {
                usb: controller,
                pin_dm: dm,
                pin_dp: dp,
            }));

            &*BUS_ALLOCATOR.as_ptr()
        };

        let usb_serial = UsbdSerialPort::new(usb_bus);
        let usb_cdc_device = UsbDeviceBuilder::new(usb_bus, UsbVidPid(0x16c0, 0x27dd))
            .product("Serial port")
            .device_class(usbd_serial::USB_CLASS_CDC)
            .build();

        CDCDevice {
            port: usb_serial,
            device: usb_cdc_device,
        }
    }

    /// Run the USB serial port, returning true if there may be data to be read.
    pub fn process(&mut self) -> bool {
        !self.device.poll(&mut [&mut self.port])
    }
}
