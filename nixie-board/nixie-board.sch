EESchema Schematic File Version 2
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:in-14
LIBS:nixie-board-cache
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L IN-14 V1
U 1 1 57E902E2
P 2400 2450
F 0 "V1" H 2300 2700 60  0000 C CNN
F 1 "IN-14" H 2250 2550 60  0000 C CNN
F 2 "in-14:IN-14" H 2400 2450 60  0001 C CNN
F 3 "" H 2400 2450 60  0001 C CNN
	1    2400 2450
	1    0    0    -1  
$EndComp
$Comp
L IN-14 V2
U 1 1 57E9045A
P 3800 2450
F 0 "V2" H 3700 2700 60  0000 C CNN
F 1 "IN-14" H 3650 2550 60  0000 C CNN
F 2 "in-14:IN-14" H 3800 2450 60  0001 C CNN
F 3 "" H 3800 2450 60  0001 C CNN
	1    3800 2450
	1    0    0    -1  
$EndComp
$Comp
L IN-14 V3
U 1 1 57E905AC
P 5200 2450
F 0 "V3" H 5100 2700 60  0000 C CNN
F 1 "IN-14" H 5050 2550 60  0000 C CNN
F 2 "in-14:IN-14" H 5200 2450 60  0001 C CNN
F 3 "" H 5200 2450 60  0001 C CNN
	1    5200 2450
	1    0    0    -1  
$EndComp
$Comp
L IN-14 V4
U 1 1 57E905B2
P 6600 2450
F 0 "V4" H 6500 2700 60  0000 C CNN
F 1 "IN-14" H 6450 2550 60  0000 C CNN
F 2 "in-14:IN-14" H 6600 2450 60  0001 C CNN
F 3 "" H 6600 2450 60  0001 C CNN
	1    6600 2450
	1    0    0    -1  
$EndComp
$Comp
L IN-14 V5
U 1 1 57E91155
P 2400 4350
F 0 "V5" H 2300 4600 60  0000 C CNN
F 1 "IN-14" H 2250 4450 60  0000 C CNN
F 2 "in-14:IN-14" H 2400 4350 60  0001 C CNN
F 3 "" H 2400 4350 60  0001 C CNN
	1    2400 4350
	1    0    0    1   
$EndComp
$Comp
L IN-14 V6
U 1 1 57E9115B
P 3800 4350
F 0 "V6" H 3700 4600 60  0000 C CNN
F 1 "IN-14" H 3650 4450 60  0000 C CNN
F 2 "in-14:IN-14" H 3800 4350 60  0001 C CNN
F 3 "" H 3800 4350 60  0001 C CNN
	1    3800 4350
	1    0    0    1   
$EndComp
$Comp
L IN-14 V7
U 1 1 57E91161
P 5200 4350
F 0 "V7" H 5100 4600 60  0000 C CNN
F 1 "IN-14" H 5050 4450 60  0000 C CNN
F 2 "in-14:IN-14" H 5200 4350 60  0001 C CNN
F 3 "" H 5200 4350 60  0001 C CNN
	1    5200 4350
	1    0    0    1   
$EndComp
$Comp
L IN-14 V8
U 1 1 57E91167
P 6600 4350
F 0 "V8" H 6500 4600 60  0000 C CNN
F 1 "IN-14" H 6450 4450 60  0000 C CNN
F 2 "in-14:IN-14" H 6600 4350 60  0001 C CNN
F 3 "" H 6600 4350 60  0001 C CNN
	1    6600 4350
	1    0    0    1   
$EndComp
$Comp
L CONN_01X04 P3
U 1 1 57E94676
P 4450 5500
F 0 "P3" H 4450 5750 50  0000 C CNN
F 1 "CONN_01X04" V 4550 5500 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x04" H 4450 5500 50  0001 C CNN
F 3 "" H 4450 5500 50  0000 C CNN
	1    4450 5500
	0    1    1    0   
$EndComp
$Comp
L CONN_01X04 P2
U 1 1 57E94771
P 4500 1300
F 0 "P2" H 4500 1550 50  0000 C CNN
F 1 "CONN_01X04" V 4600 1300 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x04" H 4500 1300 50  0001 C CNN
F 3 "" H 4500 1300 50  0000 C CNN
	1    4500 1300
	0    -1   -1   0   
$EndComp
Wire Wire Line
	1950 2900 1950 3900
Wire Wire Line
	2050 2900 2050 3900
Wire Wire Line
	2150 2900 2150 3900
Wire Wire Line
	2250 2900 2250 3900
Wire Wire Line
	2350 2900 2350 3900
Wire Wire Line
	2450 2900 2450 3900
Wire Wire Line
	2550 2900 2550 3900
Wire Wire Line
	2650 2900 2650 3900
Wire Wire Line
	2750 2900 2750 3900
Wire Wire Line
	2850 2900 2850 3900
Wire Wire Line
	3350 2900 3350 3900
Wire Wire Line
	3450 2900 3450 3900
Wire Wire Line
	3550 2900 3550 3900
Wire Wire Line
	3650 2900 3650 3900
Wire Wire Line
	3750 2900 3750 3900
Wire Wire Line
	3850 2900 3850 3900
Wire Wire Line
	3950 2900 3950 3900
Wire Wire Line
	4050 2900 4050 3900
Wire Wire Line
	4150 2900 4150 3900
Wire Wire Line
	4250 2900 4250 3900
Wire Wire Line
	4750 2900 4750 3900
Wire Wire Line
	4850 2900 4850 3900
Wire Wire Line
	4950 2900 4950 3900
Wire Wire Line
	5050 2900 5050 3900
Wire Wire Line
	5150 2900 5150 3900
Wire Wire Line
	5250 2900 5250 3900
Wire Wire Line
	5350 2900 5350 3900
Wire Wire Line
	5450 2900 5450 3900
Wire Wire Line
	5550 2900 5550 3900
Wire Wire Line
	5650 2900 5650 3900
Wire Wire Line
	6150 2900 6150 3900
Wire Wire Line
	6250 2900 6250 3900
Wire Wire Line
	6350 2900 6350 3900
Wire Wire Line
	6450 2900 6450 3900
Wire Wire Line
	6550 2900 6550 3900
Wire Wire Line
	6650 2900 6650 3900
Wire Wire Line
	6750 2900 6750 3900
Wire Wire Line
	6850 2900 6850 3900
Wire Wire Line
	6950 2900 6950 3900
Wire Wire Line
	7050 2900 7050 3900
Wire Wire Line
	1950 2950 7850 2950
Connection ~ 3350 2950
Connection ~ 1950 2950
Connection ~ 4750 2950
Connection ~ 6150 2950
Wire Wire Line
	2050 3050 7850 3050
Connection ~ 3450 3050
Connection ~ 2050 3050
Connection ~ 4850 3050
Connection ~ 6250 3050
Wire Wire Line
	2150 3150 7850 3150
Connection ~ 3550 3150
Connection ~ 2150 3150
Connection ~ 2250 3250
Wire Wire Line
	2250 3250 7850 3250
Connection ~ 3650 3250
Connection ~ 3750 3350
Connection ~ 2350 3350
Wire Wire Line
	2450 3450 7850 3450
Connection ~ 3850 3450
Connection ~ 2450 3450
Connection ~ 3950 3550
Connection ~ 2550 3550
Connection ~ 4050 3650
Connection ~ 2650 3650
Connection ~ 4150 3750
Connection ~ 2750 3750
Connection ~ 4950 3150
Connection ~ 5050 3250
Connection ~ 5150 3350
Connection ~ 5250 3450
Connection ~ 5350 3550
Connection ~ 5450 3650
Connection ~ 5550 3750
Connection ~ 4250 3850
Connection ~ 2850 3850
Connection ~ 5650 3850
Connection ~ 6350 3150
Connection ~ 6450 3250
Connection ~ 6550 3350
Connection ~ 6650 3450
Connection ~ 6750 3550
Connection ~ 6850 3650
Connection ~ 6950 3750
Connection ~ 7050 3850
Wire Wire Line
	7550 4800 2650 4800
Wire Wire Line
	7550 2000 7550 4800
Wire Wire Line
	6850 4750 6850 4800
Connection ~ 6850 4800
Wire Wire Line
	5450 4750 5450 4800
Connection ~ 5450 4800
Wire Wire Line
	4050 4750 4050 4800
Connection ~ 4050 4800
Wire Wire Line
	7650 4900 2850 4900
Wire Wire Line
	7650 1900 7650 4900
Wire Wire Line
	7050 4750 7050 4900
Connection ~ 7050 4900
Wire Wire Line
	5650 4750 5650 4900
Connection ~ 5650 4900
Wire Wire Line
	4250 4750 4250 4900
Connection ~ 4250 4900
Wire Wire Line
	2650 4800 2650 4750
Wire Wire Line
	2850 4900 2850 4750
Connection ~ 2850 4900
Wire Wire Line
	2350 3350 7850 3350
Wire Wire Line
	2550 3550 7850 3550
Wire Wire Line
	2650 3650 7850 3650
Wire Wire Line
	2750 3750 7850 3750
Wire Wire Line
	2850 3850 7850 3850
Wire Wire Line
	7550 3950 7850 3950
Wire Wire Line
	7650 4050 7850 4050
Wire Wire Line
	2650 2050 2650 2000
Wire Wire Line
	2650 2000 7550 2000
Wire Wire Line
	6850 2000 6850 2050
Connection ~ 7550 3950
Connection ~ 6850 2000
Wire Wire Line
	7050 1900 7050 2050
Wire Wire Line
	2850 1900 7650 1900
Connection ~ 7650 4050
Wire Wire Line
	5650 1900 5650 2050
Connection ~ 7050 1900
Wire Wire Line
	4250 1900 4250 2050
Connection ~ 5650 1900
Wire Wire Line
	2850 1900 2850 2050
Connection ~ 4250 1900
Wire Wire Line
	4050 2000 4050 2050
Connection ~ 4050 2000
Wire Wire Line
	5450 2000 5450 2050
Connection ~ 5450 2000
Wire Wire Line
	4750 1700 4750 2050
Wire Wire Line
	4550 1700 4750 1700
Wire Wire Line
	3350 2050 3350 1700
Wire Wire Line
	3350 1700 4450 1700
Wire Wire Line
	1950 2050 1950 1600
Wire Wire Line
	1950 1600 4350 1600
Wire Wire Line
	6150 2050 6150 1600
Wire Wire Line
	6150 1600 4650 1600
Wire Wire Line
	4350 1600 4350 1500
Wire Wire Line
	4450 1700 4450 1500
Wire Wire Line
	4550 1700 4550 1500
Wire Wire Line
	4650 1600 4650 1500
Wire Wire Line
	3350 4750 3350 5100
Wire Wire Line
	3350 5100 4400 5100
Wire Wire Line
	4750 5100 4750 4750
Wire Wire Line
	4500 5100 4750 5100
Wire Wire Line
	6150 5200 6150 4750
Wire Wire Line
	4600 5200 6150 5200
Wire Wire Line
	1950 4750 1950 5200
Wire Wire Line
	1950 5200 4300 5200
Wire Wire Line
	4300 5200 4300 5300
Wire Wire Line
	4400 5100 4400 5300
Wire Wire Line
	4500 5100 4500 5300
Wire Wire Line
	4600 5200 4600 5300
$Comp
L CONN_01X06 P1
U 1 1 58297736
P 8050 3200
F 0 "P1" H 8050 3550 50  0000 C CNN
F 1 "CONN_01X06" V 8150 3200 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x06" H 8050 3200 50  0001 C CNN
F 3 "" H 8050 3200 50  0000 C CNN
	1    8050 3200
	1    0    0    -1  
$EndComp
$Comp
L CONN_01X06 P4
U 1 1 5829777C
P 8050 3800
F 0 "P4" H 8050 4150 50  0000 C CNN
F 1 "CONN_01X06" V 8150 3800 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x06" H 8050 3800 50  0001 C CNN
F 3 "" H 8050 3800 50  0000 C CNN
	1    8050 3800
	1    0    0    -1  
$EndComp
$EndSCHEMATC
