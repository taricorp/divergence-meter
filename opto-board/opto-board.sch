EESchema Schematic File Version 4
LIBS:opto-board-cache
EELAYER 26 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L opto:TLP627-2 U2
U 1 1 583602D6
P 2150 1750
F 0 "U2" H 1850 1950 50  0000 L CNN
F 1 "TLP627-2" H 2150 1950 50  0000 L CNN
F 2 "Housings_DIP:DIP-8_W7.62mm_Socket" H 1850 1550 50  0001 L CIN
F 3 "" H 2150 1750 50  0000 L CNN
	1    2150 1750
	1    0    0    -1  
$EndComp
$Comp
L opto:TLP627-2 U2
U 2 1 58360334
P 2150 2250
F 0 "U2" H 1850 2450 50  0000 L CNN
F 1 "TLP627-2" H 2150 2450 50  0000 L CNN
F 2 "Housings_DIP:DIP-8_W7.62mm_Socket" H 1850 2050 50  0001 L CIN
F 3 "" H 2150 2250 50  0000 L CNN
	2    2150 2250
	1    0    0    -1  
$EndComp
$Comp
L conn:CONN_01X04 P4
U 1 1 58380473
P 2800 1350
F 0 "P4" H 2800 1600 50  0000 C CNN
F 1 "CONN_01X04" V 2900 1350 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x04_Pitch2.54mm" H 2800 1350 50  0001 C CNN
F 3 "" H 2800 1350 50  0000 C CNN
	1    2800 1350
	0    -1   -1   0   
$EndComp
$Comp
L conn:CONN_01X04 P5
U 1 1 583804D7
P 3350 1350
F 0 "P5" H 3350 1600 50  0000 C CNN
F 1 "CONN_01X04" V 3450 1350 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x04_Pitch2.54mm" H 3350 1350 50  0001 C CNN
F 3 "" H 3350 1350 50  0000 C CNN
	1    3350 1350
	0    -1   -1   0   
$EndComp
Text Notes 2800 1050 0    60   ~ 0
Cathodes\n(High side)
$Comp
L opto:TLP627-2 U3
U 1 1 58380CC9
P 2150 2750
F 0 "U3" H 1850 2950 50  0000 L CNN
F 1 "TLP627-2" H 2150 2950 50  0000 L CNN
F 2 "Housings_DIP:DIP-8_W7.62mm_Socket" H 1850 2550 50  0001 L CIN
F 3 "" H 2150 2750 50  0000 L CNN
	1    2150 2750
	1    0    0    -1  
$EndComp
$Comp
L opto:TLP627-2 U3
U 2 1 58380CCF
P 2150 3250
F 0 "U3" H 1850 3450 50  0000 L CNN
F 1 "TLP627-2" H 2150 3450 50  0000 L CNN
F 2 "Housings_DIP:DIP-8_W7.62mm_Socket" H 1850 3050 50  0001 L CIN
F 3 "" H 2150 3250 50  0000 L CNN
	2    2150 3250
	1    0    0    -1  
$EndComp
Text Label 1700 3600 0    60   ~ 0
V4
Text Label 1650 3700 0    60   ~ 0
V3
$Comp
L opto:TLP627-2 U5
U 1 1 58383250
P 4000 1750
F 0 "U5" H 3700 1950 50  0000 L CNN
F 1 "TLP627-2" H 4000 1950 50  0000 L CNN
F 2 "Housings_DIP:DIP-8_W7.62mm_Socket" H 3700 1550 50  0001 L CIN
F 3 "" H 4000 1750 50  0000 L CNN
	1    4000 1750
	-1   0    0    1   
$EndComp
$Comp
L opto:TLP627-2 U5
U 2 1 583833E6
P 4000 2250
F 0 "U5" H 3700 2450 50  0000 L CNN
F 1 "TLP627-2" H 4000 2450 50  0000 L CNN
F 2 "Housings_DIP:DIP-8_W7.62mm_Socket" H 3700 2050 50  0001 L CIN
F 3 "" H 4000 2250 50  0000 L CNN
	2    4000 2250
	-1   0    0    1   
$EndComp
$Comp
L opto:TLP627-2 U6
U 1 1 5838342A
P 4000 2750
F 0 "U6" H 3700 2950 50  0000 L CNN
F 1 "TLP627-2" H 4000 2950 50  0000 L CNN
F 2 "Housings_DIP:DIP-8_W7.62mm_Socket" H 3700 2550 50  0001 L CIN
F 3 "" H 4000 2750 50  0000 L CNN
	1    4000 2750
	-1   0    0    1   
$EndComp
$Comp
L opto:TLP627-2 U6
U 2 1 58383471
P 4000 3250
F 0 "U6" H 3700 3450 50  0000 L CNN
F 1 "TLP627-2" H 4000 3450 50  0000 L CNN
F 2 "Housings_DIP:DIP-8_W7.62mm_Socket" H 3700 3050 50  0001 L CIN
F 3 "" H 4000 3250 50  0000 L CNN
	2    4000 3250
	-1   0    0    1   
$EndComp
$Comp
L power:HT #PWR01
U 1 1 58384511
P 3100 3450
F 0 "#PWR01" H 3100 3570 50  0001 C CNN
F 1 "HT" H 3100 3540 50  0000 C CNN
F 2 "" H 3100 3450 50  0000 C CNN
F 3 "" H 3100 3450 50  0000 C CNN
	1    3100 3450
	1    0    0    -1  
$EndComp
Text Label 4450 3650 2    60   ~ 0
V8
Text Label 4500 3750 2    60   ~ 0
V7
Text Label 4550 3750 0    60   ~ 0
V6
Text Label 4600 3650 0    60   ~ 0
V5
$Comp
L power:VCC #PWR02
U 1 1 583873CD
P 4650 800
F 0 "#PWR02" H 4650 650 50  0001 C CNN
F 1 "VCC" H 4650 950 50  0000 C CNN
F 2 "" H 4650 800 50  0000 C CNN
F 3 "" H 4650 800 50  0000 C CNN
	1    4650 800 
	1    0    0    -1  
$EndComp
$Comp
L conn:CONN_01X06 P8
U 1 1 587F41B1
P 7600 900
F 0 "P8" H 7600 1250 50  0000 C CNN
F 1 "CONN_01X06" V 7700 900 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x06_Pitch2.54mm" H 7600 900 50  0001 C CNN
F 3 "" H 7600 900 50  0000 C CNN
	1    7600 900 
	0    -1   -1   0   
$EndComp
$Comp
L opto:TLP627-2 U7
U 1 1 587F4261
P 6650 1550
F 0 "U7" H 6350 1750 50  0000 L CNN
F 1 "TLP627-2" H 6650 1750 50  0000 L CNN
F 2 "Housings_DIP:DIP-8_W7.62mm_Socket" H 6350 1350 50  0001 L CIN
F 3 "" H 6650 1550 50  0000 L CNN
	1    6650 1550
	1    0    0    -1  
$EndComp
$Comp
L opto:TLP627-2 U7
U 2 1 587F42FD
P 6650 2050
F 0 "U7" H 6350 2250 50  0000 L CNN
F 1 "TLP627-2" H 6650 2250 50  0000 L CNN
F 2 "Housings_DIP:DIP-8_W7.62mm_Socket" H 6350 1850 50  0001 L CIN
F 3 "" H 6650 2050 50  0000 L CNN
	2    6650 2050
	1    0    0    -1  
$EndComp
Text Notes 1050 5100 1    60   ~ 0
active-low tube select
$Comp
L power:GND #PWR03
U 1 1 587F817E
P 8100 4750
F 0 "#PWR03" H 8100 4500 50  0001 C CNN
F 1 "GND" H 8100 4600 50  0000 C CNN
F 2 "" H 8100 4750 50  0000 C CNN
F 3 "" H 8100 4750 50  0000 C CNN
	1    8100 4750
	1    0    0    -1  
$EndComp
$Comp
L Device:R R7
U 1 1 587F86D1
P 8400 4500
F 0 "R7" V 8480 4500 50  0000 C CNN
F 1 "22k" V 8400 4500 50  0000 C CNN
F 2 "Resistors_THT:R_Axial_DIN0414_L11.9mm_D4.5mm_P20.32mm_Horizontal" V 8330 4500 50  0001 C CNN
F 3 "" H 8400 4500 50  0000 C CNN
	1    8400 4500
	1    0    0    -1  
$EndComp
Text Notes 7700 650  0    60   ~ 0
anodes (low side)
Text Notes 7100 5200 0    60   ~ 0
digits may share limiting resistors\nexcept LDP, RDP which permit lower current
Text Label 1600 3700 2    60   ~ 0
V2
Text Label 1550 3600 2    60   ~ 0
V1
Text Notes 7000 6850 0    60   ~ 0
Nixie switching board\n\nConnector annotations match those of the Nixie carrier board.
$Comp
L Device:R R4
U 1 1 5885DD7E
P 6200 1250
F 0 "R4" V 6280 1250 50  0000 C CNN
F 1 "220" V 6200 1250 50  0000 C CNN
F 2 "Resistors_SMD:R_0805" V 6130 1250 50  0001 C CNN
F 3 "" H 6200 1250 50  0000 C CNN
	1    6200 1250
	-1   0    0    1   
$EndComp
$Comp
L opto:TLP627-2 U8
U 1 1 5885E9F5
P 6650 2550
F 0 "U8" H 6350 2750 50  0000 L CNN
F 1 "TLP627-2" H 6650 2750 50  0000 L CNN
F 2 "Housings_DIP:DIP-8_W7.62mm_Socket" H 6350 2350 50  0001 L CIN
F 3 "" H 6650 2550 50  0000 L CNN
	1    6650 2550
	1    0    0    -1  
$EndComp
$Comp
L opto:TLP627-2 U8
U 2 1 5885E9FB
P 6650 3050
F 0 "U8" H 6350 3250 50  0000 L CNN
F 1 "TLP627-2" H 6650 3250 50  0000 L CNN
F 2 "Housings_DIP:DIP-8_W7.62mm_Socket" H 6350 2850 50  0001 L CIN
F 3 "" H 6650 3050 50  0000 L CNN
	2    6650 3050
	1    0    0    -1  
$EndComp
Text Label 7350 1250 2    60   ~ 0
1
Text Label 7450 1250 2    60   ~ 0
2
Text Label 7550 1250 2    60   ~ 0
3
Text Label 7650 1250 2    60   ~ 0
4
Text Label 7750 1250 2    60   ~ 0
5
Text Label 7850 1250 2    60   ~ 0
6
$Comp
L conn:CONN_01X06 P9
U 1 1 5885EF04
P 8700 900
F 0 "P9" H 8700 1250 50  0000 C CNN
F 1 "CONN_01X06" V 8800 900 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x06_Pitch2.54mm" H 8700 900 50  0001 C CNN
F 3 "" H 8700 900 50  0000 C CNN
	1    8700 900 
	0    -1   -1   0   
$EndComp
Text Label 8450 1250 2    60   ~ 0
7
Text Label 8550 1250 2    60   ~ 0
8
Text Label 8650 1250 2    60   ~ 0
9
Text Label 8750 1250 2    60   ~ 0
0
Text Label 8850 1250 1    60   ~ 0
LDP
Text Label 8950 1250 1    60   ~ 0
RDP
$Comp
L opto:TLP627-2 U9
U 1 1 588606C0
P 6650 3550
F 0 "U9" H 6350 3750 50  0000 L CNN
F 1 "TLP627-2" H 6650 3750 50  0000 L CNN
F 2 "Housings_DIP:DIP-8_W7.62mm_Socket" H 6350 3350 50  0001 L CIN
F 3 "" H 6650 3550 50  0000 L CNN
	1    6650 3550
	1    0    0    -1  
$EndComp
$Comp
L opto:TLP627-2 U9
U 2 1 588606C6
P 6650 4050
F 0 "U9" H 6350 4250 50  0000 L CNN
F 1 "TLP627-2" H 6650 4250 50  0000 L CNN
F 2 "Housings_DIP:DIP-8_W7.62mm_Socket" H 6350 3850 50  0001 L CIN
F 3 "" H 6650 4050 50  0000 L CNN
	2    6650 4050
	1    0    0    -1  
$EndComp
$Comp
L power:VCC #PWR04
U 1 1 58862741
P 6200 950
F 0 "#PWR04" H 6200 800 50  0001 C CNN
F 1 "VCC" H 6200 1100 50  0000 C CNN
F 2 "" H 6200 950 50  0000 C CNN
F 3 "" H 6200 950 50  0000 C CNN
	1    6200 950 
	1    0    0    -1  
$EndComp
$Comp
L opto:TLP627-2 U10
U 1 1 5886A3E8
P 9450 1550
F 0 "U10" H 9150 1750 50  0000 L CNN
F 1 "TLP627-2" H 9450 1750 50  0000 L CNN
F 2 "Housings_DIP:DIP-8_W7.62mm_Socket" H 9150 1350 50  0001 L CIN
F 3 "" H 9450 1550 50  0000 L CNN
	1    9450 1550
	-1   0    0    1   
$EndComp
$Comp
L opto:TLP627-2 U10
U 2 1 5886D489
P 9450 2050
F 0 "U10" H 9150 2250 50  0000 L CNN
F 1 "TLP627-2" H 9450 2250 50  0000 L CNN
F 2 "Housings_DIP:DIP-8_W7.62mm_Socket" H 9150 1850 50  0001 L CIN
F 3 "" H 9450 2050 50  0000 L CNN
	2    9450 2050
	-1   0    0    1   
$EndComp
$Comp
L opto:TLP627-2 U11
U 1 1 5886D6A7
P 9450 2550
F 0 "U11" H 9150 2750 50  0000 L CNN
F 1 "TLP627-2" H 9450 2750 50  0000 L CNN
F 2 "Housings_DIP:DIP-8_W7.62mm_Socket" H 9150 2350 50  0001 L CIN
F 3 "" H 9450 2550 50  0000 L CNN
	1    9450 2550
	-1   0    0    1   
$EndComp
$Comp
L opto:TLP627-2 U11
U 2 1 5886D6AD
P 9450 3050
F 0 "U11" H 9150 3250 50  0000 L CNN
F 1 "TLP627-2" H 9450 3250 50  0000 L CNN
F 2 "Housings_DIP:DIP-8_W7.62mm_Socket" H 9150 2850 50  0001 L CIN
F 3 "" H 9450 3050 50  0000 L CNN
	2    9450 3050
	-1   0    0    1   
$EndComp
$Comp
L opto:TLP627-2 U12
U 1 1 5886DC10
P 9450 3550
F 0 "U12" H 9150 3750 50  0000 L CNN
F 1 "TLP627-2" H 9450 3750 50  0000 L CNN
F 2 "Housings_DIP:DIP-8_W7.62mm_Socket" H 9150 3350 50  0001 L CIN
F 3 "" H 9450 3550 50  0000 L CNN
	1    9450 3550
	-1   0    0    1   
$EndComp
$Comp
L opto:TLP627-2 U12
U 2 1 5886DC16
P 9450 4050
F 0 "U12" H 9150 4250 50  0000 L CNN
F 1 "TLP627-2" H 9450 4250 50  0000 L CNN
F 2 "Housings_DIP:DIP-8_W7.62mm_Socket" H 9150 3850 50  0001 L CIN
F 3 "" H 9450 4050 50  0000 L CNN
	2    9450 4050
	-1   0    0    1   
$EndComp
$Comp
L Device:R R6
U 1 1 5887297D
P 8200 4500
F 0 "R6" V 8280 4500 50  0000 C CNN
F 1 "47k" V 8200 4500 50  0000 C CNN
F 2 "Resistors_THT:R_Axial_DIN0414_L11.9mm_D4.5mm_P20.32mm_Horizontal" V 8130 4500 50  0001 C CNN
F 3 "" H 8200 4500 50  0000 C CNN
	1    8200 4500
	1    0    0    -1  
$EndComp
$Comp
L Device:R R8
U 1 1 58876824
P 9900 1250
F 0 "R8" V 9980 1250 50  0000 C CNN
F 1 "220" V 9900 1250 50  0000 C CNN
F 2 "Resistors_SMD:R_0805" V 9830 1250 50  0001 C CNN
F 3 "" H 9900 1250 50  0000 C CNN
	1    9900 1250
	1    0    0    1   
$EndComp
$Comp
L power:VCC #PWR05
U 1 1 5887682A
P 9900 1000
F 0 "#PWR05" H 9900 850 50  0001 C CNN
F 1 "VCC" H 9900 1150 50  0000 C CNN
F 2 "" H 9900 1000 50  0000 C CNN
F 3 "" H 9900 1000 50  0000 C CNN
	1    9900 1000
	-1   0    0    -1  
$EndComp
$Comp
L Device:R R3
U 1 1 58878865
P 4650 1000
F 0 "R3" V 4730 1000 50  0000 C CNN
F 1 "220" V 4650 1000 50  0000 C CNN
F 2 "Resistors_SMD:R_0805" V 4580 1000 50  0001 C CNN
F 3 "" H 4650 1000 50  0000 C CNN
	1    4650 1000
	-1   0    0    1   
$EndComp
$Comp
L conn:CONN_01X04 P2
U 1 1 5887C314
P 1700 4200
F 0 "P2" H 1700 4450 50  0000 C CNN
F 1 "CONN_01X04" V 1800 4200 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x04_Pitch2.54mm" H 1700 4200 50  0001 C CNN
F 3 "" H 1700 4200 50  0000 C CNN
	1    1700 4200
	0    -1   1    0   
$EndComp
$Comp
L conn:CONN_01X04 P6
U 1 1 5887D203
P 4450 4200
F 0 "P6" H 4450 4450 50  0000 C CNN
F 1 "CONN_01X04" V 4550 4200 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x04_Pitch2.54mm" H 4450 4200 50  0001 C CNN
F 3 "" H 4450 4200 50  0000 C CNN
	1    4450 4200
	0    1    1    0   
$EndComp
$Comp
L conn:CONN_01X06 P7
U 1 1 5887F68F
P 6150 4950
F 0 "P7" H 6150 5300 50  0000 C CNN
F 1 "CONN_01X06" V 6250 4950 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x06_Pitch2.54mm" H 6150 4950 50  0001 C CNN
F 3 "" H 6150 4950 50  0000 C CNN
	1    6150 4950
	0    -1   1    0   
$EndComp
$Comp
L conn:CONN_01X06 P10
U 1 1 58881FEB
P 9950 4950
F 0 "P10" H 9950 5300 50  0000 C CNN
F 1 "CONN_01X06" V 10050 4950 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x06_Pitch2.54mm" H 9950 4950 50  0001 C CNN
F 3 "" H 9950 4950 50  0000 C CNN
	1    9950 4950
	0    1    1    0   
$EndComp
Text Notes 5100 750  0    118  ~ 0
Nixie Side
Text Notes 5050 5450 0    118  ~ 0
Control Side
$Comp
L Device:R R5
U 1 1 588C716F
P 8000 4500
F 0 "R5" V 8080 4500 50  0000 C CNN
F 1 "22k" V 8000 4500 50  0000 C CNN
F 2 "Resistors_THT:R_Axial_DIN0414_L11.9mm_D4.5mm_P20.32mm_Horizontal" V 7930 4500 50  0001 C CNN
F 3 "" H 8000 4500 50  0000 C CNN
	1    8000 4500
	1    0    0    -1  
$EndComp
$Comp
L conn:CONN_01X01 H1
U 1 1 588F02DC
P 2800 6750
F 0 "H1" H 2800 6850 50  0000 C CNN
F 1 "CONN_01X01" V 2900 6750 50  0000 C CNN
F 2 "Mounting_Holes:MountingHole_3.2mm_M3_Pad" H 2800 6750 50  0001 C CNN
F 3 "" H 2800 6750 50  0000 C CNN
	1    2800 6750
	0    -1   -1   0   
$EndComp
$Comp
L conn:CONN_01X01 H2
U 1 1 588F0549
P 3000 6750
F 0 "H2" H 3000 6850 50  0000 C CNN
F 1 "CONN_01X01" V 3100 6750 50  0000 C CNN
F 2 "Mounting_Holes:MountingHole_3.2mm_M3_Pad" H 3000 6750 50  0001 C CNN
F 3 "" H 3000 6750 50  0000 C CNN
	1    3000 6750
	0    -1   -1   0   
$EndComp
$Comp
L conn:CONN_01X01 H3
U 1 1 588F0602
P 3200 6750
F 0 "H3" H 3200 6850 50  0000 C CNN
F 1 "CONN_01X01" V 3300 6750 50  0000 C CNN
F 2 "Mounting_Holes:MountingHole_3.2mm_M3_Pad" H 3200 6750 50  0001 C CNN
F 3 "" H 3200 6750 50  0000 C CNN
	1    3200 6750
	0    -1   -1   0   
$EndComp
$Comp
L conn:CONN_01X01 H4
U 1 1 588F06AA
P 3400 6750
F 0 "H4" H 3400 6850 50  0000 C CNN
F 1 "CONN_01X01" V 3500 6750 50  0000 C CNN
F 2 "Mounting_Holes:MountingHole_3.2mm_M3_Pad" H 3400 6750 50  0001 C CNN
F 3 "" H 3400 6750 50  0000 C CNN
	1    3400 6750
	0    -1   -1   0   
$EndComp
$Comp
L power:GND #PWR06
U 1 1 588F072A
P 3100 7050
F 0 "#PWR06" H 3100 6800 50  0001 C CNN
F 1 "GND" H 3100 6900 50  0000 C CNN
F 2 "" H 3100 7050 50  0000 C CNN
F 3 "" H 3100 7050 50  0000 C CNN
	1    3100 7050
	1    0    0    -1  
$EndComp
Text Notes 2800 7450 0    60   ~ 0
Mounting holes\nM3, 3.2mm
$Comp
L conn:CONN_01X02 P1
U 1 1 58900048
P 1400 6750
F 0 "P1" H 1400 6900 50  0000 C CNN
F 1 "CONN_01X02" V 1500 6750 50  0000 C CNN
F 2 "tari:Pin_Header_Straight_1x02_Pitch3.81mm" H 1400 6750 50  0001 C CNN
F 3 "" H 1400 6750 50  0000 C CNN
	1    1400 6750
	0    -1   -1   0   
$EndComp
$Comp
L power:GND #PWR07
U 1 1 58900C25
P 1450 7100
F 0 "#PWR07" H 1450 6850 50  0001 C CNN
F 1 "GND" H 1450 6950 50  0000 C CNN
F 2 "" H 1450 7100 50  0000 C CNN
F 3 "" H 1450 7100 50  0000 C CNN
	1    1450 7100
	1    0    0    -1  
$EndComp
$Comp
L power:HT #PWR08
U 1 1 5890167D
P 1350 7100
F 0 "#PWR08" H 1350 7220 50  0001 C CNN
F 1 "HT" H 1350 7190 50  0000 C CNN
F 2 "" H 1350 7100 50  0000 C CNN
F 3 "" H 1350 7100 50  0000 C CNN
	1    1350 7100
	-1   0    0    1   
$EndComp
$Comp
L conn:CONN_01X03 P3
U 1 1 5890253F
P 2150 6800
F 0 "P3" H 2150 7000 50  0000 C CNN
F 1 "CONN_01X03" V 2250 6800 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x03_Pitch2.54mm" H 2150 6800 50  0001 C CNN
F 3 "" H 2150 6800 50  0000 C CNN
	1    2150 6800
	0    -1   -1   0   
$EndComp
$Comp
L power:GND #PWR09
U 1 1 5890370E
P 2250 7050
F 0 "#PWR09" H 2250 6800 50  0001 C CNN
F 1 "GND" H 2250 6900 50  0000 C CNN
F 2 "" H 2250 7050 50  0000 C CNN
F 3 "" H 2250 7050 50  0000 C CNN
	1    2250 7050
	1    0    0    -1  
$EndComp
$Comp
L power:VCC #PWR010
U 1 1 589038C8
P 2150 7250
F 0 "#PWR010" H 2150 7100 50  0001 C CNN
F 1 "VCC" H 2150 7400 50  0000 C CNN
F 2 "" H 2150 7250 50  0000 C CNN
F 3 "" H 2150 7250 50  0000 C CNN
	1    2150 7250
	-1   0    0    1   
$EndComp
Text Notes 2250 6500 0    60   ~ 0
Power
Text Notes 7400 5500 0    60   ~ 0
Digits: 2 mA, 10 mA pulsed (1 kHz)\nDPs: 0.5 mA, 4 mA pulsed
$Comp
L opto-board-rescue:74LS30 U1
U 1 1 590463FE
P 2000 4850
F 0 "U1" H 2000 4950 50  0000 C CNN
F 1 "74HCT30" H 2000 4750 50  0000 C CNN
F 2 "Housings_SSOP:TSSOP-14_4.4x5mm_Pitch0.65mm" H 2000 4850 50  0001 C CNN
F 3 "" H 2000 4850 50  0000 C CNN
	1    2000 4850
	1    0    0    -1  
$EndComp
$Comp
L opto:TLP127 U4
U 1 1 5906B418
P 3050 4750
F 0 "U4" H 2750 4950 50  0000 L CNN
F 1 "TLP187" H 3050 4950 50  0000 L CNN
F 2 "tari:SO6-4PIN" H 2750 4550 50  0001 L CIN
F 3 "" H 3050 4750 50  0000 L CNN
	1    3050 4750
	1    0    0    -1  
$EndComp
$Comp
L Device:R R2
U 1 1 5906DCD7
P 2800 4300
F 0 "R2" V 2880 4300 50  0000 C CNN
F 1 "220" V 2800 4300 50  0000 C CNN
F 2 "Resistors_SMD:R_0805" V 2730 4300 50  0001 C CNN
F 3 "" H 2800 4300 50  0000 C CNN
	1    2800 4300
	1    0    0    1   
$EndComp
$Comp
L power:GND #PWR011
U 1 1 5906FC9C
P 3500 4900
F 0 "#PWR011" H 3500 4650 50  0001 C CNN
F 1 "GND" H 3500 4750 50  0000 C CNN
F 2 "" H 3500 4900 50  0000 C CNN
F 3 "" H 3500 4900 50  0000 C CNN
	1    3500 4900
	1    0    0    -1  
$EndComp
$Comp
L power:VCC #PWR012
U 1 1 59074E79
P 2250 4600
F 0 "#PWR012" H 2250 4450 50  0001 C CNN
F 1 "VCC" H 2250 4750 50  0000 C CNN
F 2 "" H 2250 4600 50  0000 C CNN
F 3 "" H 2250 4600 50  0000 C CNN
	1    2250 4600
	1    0    0    -1  
$EndComp
$Comp
L Device:R R1
U 1 1 59074F2A
P 2450 4650
F 0 "R1" V 2530 4650 50  0000 C CNN
F 1 "220" V 2450 4650 50  0000 C CNN
F 2 "Resistors_SMD:R_0805" V 2380 4650 50  0001 C CNN
F 3 "" H 2450 4650 50  0000 C CNN
	1    2450 4650
	0    1    1    0   
$EndComp
Entry Wire Line
	4700 3800 4800 3900
Entry Wire Line
	4700 3850 4800 3950
Entry Wire Line
	4700 3900 4800 4000
Entry Wire Line
	4700 3950 4800 4050
Entry Wire Line
	1250 5200 1350 5100
Entry Wire Line
	1250 5000 1350 4900
Entry Wire Line
	1250 5300 1350 5200
Entry Wire Line
	1250 5100 1350 5000
Text Label 1350 4500 0    60   ~ 0
V5
Text Label 1350 4600 0    60   ~ 0
V6
Text Label 1350 4700 0    60   ~ 0
V7
Text Label 1350 4800 0    60   ~ 0
V8
$Comp
L tari:D_x2_ComC D1
U 1 1 590B73AC
P 2700 3900
F 0 "D1" H 2775 3750 50  0000 C CNN
F 1 "D_x2_ComC" V 2550 3900 50  0001 C CNN
F 2 "TO_SOT_Packages_SMD:SOT-23" H 2700 3775 50  0001 C CNN
F 3 "" H 2700 3775 50  0000 C CNN
	1    2700 3900
	1    0    0    -1  
$EndComp
$Comp
L tari:D_x2_ComC D2
U 1 1 590B7555
P 2900 3900
F 0 "D2" H 2975 3750 50  0000 C CNN
F 1 "D_x2_ComC" V 2750 3900 50  0001 C CNN
F 2 "TO_SOT_Packages_SMD:SOT-23" H 2900 3775 50  0001 C CNN
F 3 "" H 2900 3775 50  0000 C CNN
	1    2900 3900
	1    0    0    -1  
$EndComp
$Comp
L tari:D_x2_ComC D3
U 1 1 590B7948
P 3250 3900
F 0 "D3" H 3325 3750 50  0000 C CNN
F 1 "D_x2_ComC" V 3100 3900 50  0001 C CNN
F 2 "TO_SOT_Packages_SMD:SOT-23" H 3250 3775 50  0001 C CNN
F 3 "" H 3250 3775 50  0000 C CNN
	1    3250 3900
	1    0    0    -1  
$EndComp
$Comp
L tari:D_x2_ComC D4
U 1 1 590B79F2
P 3450 3900
F 0 "D4" H 3525 3750 50  0000 C CNN
F 1 "D_x2_ComC" V 3300 3900 50  0001 C CNN
F 2 "TO_SOT_Packages_SMD:SOT-23" H 3450 3775 50  0001 C CNN
F 3 "" H 3450 3775 50  0000 C CNN
	1    3450 3900
	1    0    0    -1  
$EndComp
Text Notes 2550 4250 1    60   ~ 0
MMBD3004C x4
$Comp
L Device:R R9
U 1 1 590C2B18
P 3350 4300
F 0 "R9" V 3430 4300 50  0000 C CNN
F 1 "220" V 3350 4300 50  0000 C CNN
F 2 "Resistors_SMD:R_0805" V 3280 4300 50  0001 C CNN
F 3 "" H 3350 4300 50  0000 C CNN
	1    3350 4300
	-1   0    0    1   
$EndComp
Entry Wire Line
	1250 4900 1350 4800
Entry Wire Line
	1250 4800 1350 4700
Entry Wire Line
	1250 4700 1350 4600
Entry Wire Line
	1250 4600 1350 4500
Entry Wire Line
	1250 4050 1350 3950
Entry Wire Line
	1250 4000 1350 3900
Entry Wire Line
	1250 3950 1350 3850
Entry Wire Line
	1250 3900 1350 3800
Text Label 1350 5200 0    60   ~ 0
V3
Text Label 1350 5100 0    60   ~ 0
V4
Text Label 1350 4900 0    60   ~ 0
V1
Text Label 1350 5000 0    60   ~ 0
V2
Text Notes 1500 3050 1    60   ~ 0
NEED PULL-UP NETWORKS
$Comp
L conn:CONN_01X02 P11
U 1 1 593E6808
P 900 6750
F 0 "P11" H 900 6900 50  0000 C CNN
F 1 "CONN_01X02" V 1000 6750 50  0000 C CNN
F 2 "tari:Pin_Header_Straight_1x02_Pitch3.81mm" H 900 6750 50  0001 C CNN
F 3 "" H 900 6750 50  0000 C CNN
	1    900  6750
	0    -1   -1   0   
$EndComp
Wire Wire Line
	2550 2350 2750 2350
Wire Wire Line
	2600 2150 2550 2150
Wire Wire Line
	2850 1550 2850 2850
Wire Wire Line
	2850 2850 2550 2850
Wire Wire Line
	2950 3350 2550 3350
Wire Wire Line
	2600 2650 2550 2650
Connection ~ 2600 2150
Wire Wire Line
	2600 3150 2550 3150
Connection ~ 2600 2650
Connection ~ 2600 3150
Wire Wire Line
	3600 1650 3200 1650
Wire Wire Line
	3600 2150 3300 2150
Wire Wire Line
	3300 1550 3300 2150
Wire Wire Line
	3600 2650 3400 2650
Wire Wire Line
	3600 3150 3500 3150
Wire Wire Line
	3500 1550 3500 3150
Wire Wire Line
	3600 1850 3550 1850
Wire Wire Line
	3550 1850 3550 2350
Wire Wire Line
	3600 3350 3550 3350
Connection ~ 3550 3350
Wire Wire Line
	3600 2850 3550 2850
Connection ~ 3550 2850
Wire Wire Line
	3600 2350 3550 2350
Connection ~ 3550 2350
Wire Wire Line
	3550 3550 3100 3550
Wire Wire Line
	4450 1650 4450 3800
Wire Wire Line
	4500 2150 4500 3850
Wire Wire Line
	4550 2650 4550 3900
Wire Wire Line
	4600 3150 4600 3950
Wire Wire Line
	1550 1850 1550 3950
Wire Wire Line
	1600 2350 1600 3900
Wire Wire Line
	1650 2850 1650 3850
Wire Wire Line
	1700 3350 1700 3800
Wire Wire Line
	7050 1450 7350 1450
Wire Wire Line
	7350 1450 7350 1100
Wire Wire Line
	7050 1950 7450 1950
Wire Wire Line
	7450 1950 7450 1100
Wire Wire Line
	7050 1650 8000 1650
Wire Wire Line
	8000 2150 7050 2150
Connection ~ 8000 2150
Wire Wire Line
	8000 1650 8000 2150
Wire Wire Line
	3100 3450 3100 3550
Connection ~ 3100 3550
Wire Wire Line
	1700 1200 1700 1650
Connection ~ 1700 2650
Connection ~ 1700 2150
Connection ~ 1700 1650
Wire Wire Line
	1700 2150 1750 2150
Wire Wire Line
	1700 3150 1750 3150
Wire Wire Line
	1750 2650 1700 2650
Wire Wire Line
	4650 3350 4400 3350
Connection ~ 4650 1850
Wire Wire Line
	4650 2350 4400 2350
Connection ~ 4650 2350
Wire Wire Line
	4650 2850 4400 2850
Connection ~ 4650 2850
Wire Wire Line
	7550 1100 7550 2450
Wire Wire Line
	7550 2450 7050 2450
Wire Wire Line
	7650 1100 7650 2950
Wire Wire Line
	7650 2950 7050 2950
Wire Wire Line
	7050 2650 8000 2650
Connection ~ 8000 2650
Wire Wire Line
	7050 3150 8000 3150
Connection ~ 8000 3150
Wire Wire Line
	7050 3450 7750 3450
Wire Wire Line
	7050 3950 7850 3950
Wire Wire Line
	7750 3450 7750 1100
Wire Wire Line
	7850 3950 7850 1100
Wire Wire Line
	7050 3650 8000 3650
Connection ~ 8000 3650
Wire Wire Line
	7050 4150 8000 4150
Connection ~ 8000 4150
Wire Wire Line
	6200 1400 6200 1450
Wire Wire Line
	6200 1450 6250 1450
Wire Wire Line
	6200 1950 6250 1950
Connection ~ 6200 1450
Wire Wire Line
	6200 2450 6250 2450
Connection ~ 6200 1950
Wire Wire Line
	6200 2950 6250 2950
Connection ~ 6200 2450
Connection ~ 6200 2950
Wire Wire Line
	6200 3450 6250 3450
Wire Wire Line
	6200 3950 6250 3950
Connection ~ 6200 3450
Wire Wire Line
	6200 950  6200 1100
Wire Wire Line
	9050 3450 8200 3450
Wire Wire Line
	8200 3450 8200 3950
Wire Wire Line
	9050 3950 8200 3950
Connection ~ 8200 3950
Wire Wire Line
	8450 1100 8450 1650
Wire Wire Line
	8450 1650 9050 1650
Wire Wire Line
	8550 1100 8550 2150
Wire Wire Line
	8550 2150 9050 2150
Wire Wire Line
	8650 1100 8650 2650
Wire Wire Line
	8650 2650 9050 2650
Wire Wire Line
	8750 1100 8750 3150
Wire Wire Line
	8750 3150 9050 3150
Wire Wire Line
	8850 1100 8850 3650
Wire Wire Line
	8850 3650 9050 3650
Wire Wire Line
	8950 1100 8950 4150
Wire Wire Line
	8950 4150 9050 4150
Wire Wire Line
	8000 4700 8100 4700
Wire Wire Line
	8100 4700 8100 4750
Wire Wire Line
	8000 4650 8000 4700
Wire Wire Line
	8200 4700 8200 4650
Connection ~ 8100 4700
Wire Wire Line
	9900 1400 9900 1650
Wire Wire Line
	9900 1000 9900 1100
Wire Wire Line
	9950 3950 9950 4500
Wire Wire Line
	10000 3450 10000 4550
Wire Wire Line
	10050 2950 10050 4600
Wire Wire Line
	10150 1950 10150 4700
Wire Wire Line
	10200 1450 10200 4750
Wire Wire Line
	4650 1850 4400 1850
Wire Wire Line
	1650 3900 1650 4000
Wire Wire Line
	1750 3850 1750 4000
Wire Wire Line
	1850 3800 1850 4000
Wire Wire Line
	4500 3900 4550 3900
Wire Wire Line
	4500 3900 4500 4000
Wire Wire Line
	4400 3850 4500 3850
Wire Wire Line
	4400 3850 4400 4000
Wire Wire Line
	4300 3800 4450 3800
Wire Wire Line
	4300 3800 4300 4000
Wire Wire Line
	6000 4750 6000 4700
Wire Wire Line
	6000 4700 5950 4700
Wire Wire Line
	6100 4750 6100 4650
Wire Wire Line
	6100 4650 6000 4650
Wire Wire Line
	6200 4750 6200 4600
Wire Wire Line
	6200 4600 6050 4600
Wire Wire Line
	6300 4750 6300 4550
Wire Wire Line
	6300 4550 6100 4550
Wire Wire Line
	6400 4750 6400 4500
Wire Wire Line
	6400 4500 6150 4500
Wire Wire Line
	10100 4750 10100 4700
Wire Wire Line
	10100 4700 10150 4700
Wire Wire Line
	10000 4750 10000 4650
Wire Wire Line
	10000 4650 10100 4650
Wire Wire Line
	9900 4750 9900 4600
Wire Wire Line
	9900 4600 10050 4600
Wire Wire Line
	9800 4750 9800 4550
Wire Wire Line
	9800 4550 10000 4550
Wire Wire Line
	9700 4750 9700 4500
Wire Wire Line
	9700 4500 9950 4500
Wire Wire Line
	8400 1450 8400 1950
Wire Wire Line
	8400 2450 9050 2450
Wire Wire Line
	9050 2950 8400 2950
Connection ~ 8400 2950
Wire Wire Line
	8400 4700 8400 4650
Connection ~ 8200 4700
Connection ~ 8400 1950
Connection ~ 8400 2450
Wire Wire Line
	8400 1950 9050 1950
Wire Wire Line
	9050 1450 8400 1450
Wire Wire Line
	9850 1650 9900 1650
Connection ~ 9900 1650
Wire Wire Line
	9900 4150 9850 4150
Wire Wire Line
	9850 3650 9900 3650
Connection ~ 9900 3650
Wire Wire Line
	9850 3150 9900 3150
Connection ~ 9900 3150
Wire Wire Line
	9850 2650 9900 2650
Connection ~ 9900 2650
Wire Wire Line
	9850 2150 9900 2150
Connection ~ 9900 2150
Wire Wire Line
	5900 1650 6250 1650
Wire Wire Line
	6250 2150 5950 2150
Wire Wire Line
	6250 4150 6150 4150
Wire Wire Line
	6250 3650 6100 3650
Wire Wire Line
	6250 3150 6050 3150
Wire Wire Line
	6250 2650 6000 2650
Wire Wire Line
	2800 6950 2800 7000
Wire Wire Line
	2800 7000 3000 7000
Wire Wire Line
	3100 7000 3100 7050
Wire Wire Line
	3000 6950 3000 7000
Connection ~ 3000 7000
Wire Wire Line
	3200 7000 3200 6950
Connection ~ 3100 7000
Wire Wire Line
	3400 7000 3400 6950
Connection ~ 3200 7000
Wire Wire Line
	1700 1650 1750 1650
Wire Wire Line
	4650 1150 4650 1200
Connection ~ 4650 1200
Wire Wire Line
	1750 1850 1550 1850
Wire Wire Line
	1450 6950 1450 7000
Wire Wire Line
	1350 7100 1350 7050
Wire Wire Line
	2050 7000 2050 7050
Wire Wire Line
	2050 7050 2250 7050
Wire Wire Line
	2250 7050 2250 7000
Wire Wire Line
	2150 7000 2150 7250
Wire Wire Line
	2650 1550 2650 1850
Wire Wire Line
	2600 3550 2600 3150
Wire Wire Line
	2600 1650 2550 1650
Wire Wire Line
	2550 1850 2650 1850
Wire Wire Line
	1600 2350 1750 2350
Wire Wire Line
	1700 3350 1750 3350
Wire Wire Line
	1650 2850 1750 2850
Wire Wire Line
	4600 3150 4400 3150
Wire Wire Line
	4550 2650 4400 2650
Wire Wire Line
	4500 2150 4400 2150
Wire Wire Line
	4450 1650 4400 1650
Wire Wire Line
	4650 800  4650 850 
Wire Wire Line
	4650 1200 1700 1200
Connection ~ 2650 1850
Connection ~ 2750 2350
Connection ~ 2850 2850
Connection ~ 2950 3350
Connection ~ 3500 3150
Connection ~ 3400 2650
Connection ~ 3300 2150
Connection ~ 3200 1650
Wire Wire Line
	3500 4500 3500 4650
Wire Wire Line
	3500 4650 3450 4650
Wire Wire Line
	3500 4900 3500 4850
Wire Wire Line
	3500 4850 3450 4850
Wire Wire Line
	2250 4600 2250 4650
Wire Wire Line
	2250 4650 2300 4650
Wire Wire Line
	2600 4650 2650 4650
Wire Wire Line
	2600 4850 2650 4850
Connection ~ 1550 3950
Connection ~ 1600 3900
Connection ~ 1650 3850
Connection ~ 1700 3800
Connection ~ 4450 3800
Connection ~ 4500 3850
Connection ~ 4550 3900
Wire Wire Line
	4700 3950 4600 3950
Connection ~ 4600 3950
Wire Bus Line
	4800 5350 1250 5350
Wire Wire Line
	1350 4900 1400 4900
Wire Wire Line
	1350 5200 1400 5200
Wire Wire Line
	1400 5100 1350 5100
Wire Wire Line
	1400 5000 1350 5000
Wire Wire Line
	5900 1650 5900 4750
Wire Wire Line
	5950 2150 5950 4700
Wire Wire Line
	6000 2650 6000 4650
Wire Wire Line
	6050 3150 6050 4600
Wire Wire Line
	6100 3650 6100 4550
Wire Wire Line
	6150 4150 6150 4500
Wire Wire Line
	9850 1450 10200 1450
Wire Wire Line
	9850 1950 10150 1950
Wire Wire Line
	9850 2450 10100 2450
Wire Wire Line
	9850 2950 10050 2950
Wire Wire Line
	9850 3450 10000 3450
Wire Wire Line
	9850 3950 9950 3950
Wire Wire Line
	10100 2450 10100 4650
Wire Wire Line
	2750 1550 2750 2350
Wire Wire Line
	2950 1550 2950 3350
Wire Wire Line
	3200 1550 3200 1650
Wire Wire Line
	3400 1550 3400 2650
Wire Wire Line
	2700 4150 2700 4100
Wire Wire Line
	2900 4150 2900 4100
Wire Wire Line
	3250 4100 3250 4150
Wire Wire Line
	3450 4150 3450 4100
Wire Wire Line
	3250 4150 3350 4150
Wire Wire Line
	2700 4150 2800 4150
Connection ~ 2800 4150
Connection ~ 3350 4150
Wire Wire Line
	2800 4450 2800 4500
Wire Wire Line
	2800 4500 3350 4500
Wire Wire Line
	3350 4500 3350 4450
Connection ~ 3350 4500
Wire Wire Line
	1350 4800 1400 4800
Wire Wire Line
	1350 4700 1400 4700
Wire Wire Line
	1400 4600 1350 4600
Wire Wire Line
	1350 4500 1400 4500
Wire Wire Line
	1350 3800 1700 3800
Wire Wire Line
	1350 3850 1650 3850
Wire Wire Line
	1350 3900 1600 3900
Wire Wire Line
	1350 3950 1550 3950
Wire Wire Line
	950  6950 950  7000
Wire Wire Line
	950  7000 1450 7000
Connection ~ 1450 7000
Wire Wire Line
	850  6950 850  7050
Wire Wire Line
	850  7050 1350 7050
Connection ~ 1350 7050
Wire Wire Line
	2600 2150 2600 1650
Wire Wire Line
	2600 2650 2600 2150
Wire Wire Line
	2600 3150 2600 2650
Wire Wire Line
	3550 3350 3550 3550
Wire Wire Line
	3550 2850 3550 3350
Wire Wire Line
	3550 2350 3550 2850
Wire Wire Line
	8000 2150 8000 2650
Wire Wire Line
	3100 3550 2600 3550
Wire Wire Line
	1700 2650 1700 3150
Wire Wire Line
	1700 2150 1700 2650
Wire Wire Line
	1700 1650 1700 2150
Wire Wire Line
	4650 1850 4650 2350
Wire Wire Line
	4650 2350 4650 2850
Wire Wire Line
	4650 2850 4650 3350
Wire Wire Line
	8000 2650 8000 3150
Wire Wire Line
	8000 4150 8000 4350
Wire Wire Line
	6200 1450 6200 1950
Wire Wire Line
	6200 1950 6200 2450
Wire Wire Line
	6200 2450 6200 2950
Wire Wire Line
	6200 2950 6200 3450
Wire Wire Line
	6200 3450 6200 3950
Wire Wire Line
	8200 3950 8200 4350
Wire Wire Line
	8100 4700 8200 4700
Wire Wire Line
	8400 2950 8400 4350
Wire Wire Line
	8200 4700 8400 4700
Wire Wire Line
	8400 1950 8400 2450
Wire Wire Line
	8400 2450 8400 2950
Wire Wire Line
	9900 1650 9900 2150
Wire Wire Line
	9900 3650 9900 4150
Wire Wire Line
	9900 3150 9900 3650
Wire Wire Line
	9900 2650 9900 3150
Wire Wire Line
	9900 2150 9900 2650
Wire Wire Line
	3000 7000 3100 7000
Wire Wire Line
	3100 7000 3200 7000
Wire Wire Line
	3200 7000 3400 7000
Wire Wire Line
	4650 1200 4650 1850
Wire Wire Line
	2650 1850 2650 3700
Wire Wire Line
	2750 2350 2750 3700
Wire Wire Line
	2850 2850 2850 3700
Wire Wire Line
	2950 3350 2950 3700
Wire Wire Line
	3500 3150 3500 3700
Wire Wire Line
	3400 2650 3400 3700
Wire Wire Line
	3300 2150 3300 3700
Wire Wire Line
	3200 1650 3200 3700
Wire Wire Line
	1550 3950 1550 4000
Wire Wire Line
	1600 3900 1650 3900
Wire Wire Line
	1650 3850 1750 3850
Wire Wire Line
	1700 3800 1850 3800
Wire Wire Line
	4450 3800 4700 3800
Wire Wire Line
	4500 3850 4700 3850
Wire Wire Line
	4550 3900 4700 3900
Wire Wire Line
	4600 3950 4600 4000
Wire Wire Line
	2800 4150 2900 4150
Wire Wire Line
	3350 4150 3450 4150
Wire Wire Line
	3350 4500 3500 4500
Wire Wire Line
	1450 7000 1450 7100
Wire Wire Line
	1350 7050 1350 6950
Wire Wire Line
	8000 3150 8000 3650
Wire Wire Line
	8000 3650 8000 4150
Wire Bus Line
	4800 3900 4800 5350
Wire Bus Line
	1250 3900 1250 5350
Text Notes 4700 6450 0    100  ~ 20
TLP627-2 is obsolete\nmay substitute CPC1302G
$EndSCHEMATC
